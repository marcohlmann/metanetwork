---
title: "Metanetwork, a R package to handle and represent metanetworks"
author: "Marc Ohlmann, Jimmy Garnier, Laurent Vuillon"
date: "1/6/2021"
output:
  slidy_presentation:
    fig_height: 8
    fig_width: 10
---

```{r setup, include=FALSE}
library(GGally)
library(network)
library(sna)
library(ggplot2)
library(intergraph)
library(dplyr)
library(igraph)
library(Matrix)
library(Matrix.utils)
library(ade4)
library(visNetwork)
```


\small
## Representing networks ?
- Problem of node layouts
- Add physics to nodes ?
- In ecology, XXX

## Metanetwork

- metacommunity, metanetwork
- conveniant case: metaweb

## Connected and disconnected graphs

- local networks might be disconnected

## metanetwork package: a raw use
\small
```{r,echo = TRUE}
#generate a directed lattice
g = igraph::make_lattice(dim = 2,length = 8,directed = T)
n = vcount(g)
#node names
V(g)$name = as.character(1:n)
```

## metanetwork package: a raw use
```{r,echo = TRUE}
#sampling a presence table
presence = rbind(rbinom(n,size = 1,prob = 0.5),
                 rbinom(n,size = 1,prob = 0.8),
                 rep(1,n))

rownames(presence) = c('a','b','c')
colnames(presence) = V(g)$name
```

## Changing ggnet parameters

\small
```{r,echo = TRUE,warning = FALSE}
print('tt')
```

